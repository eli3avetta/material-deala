import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { AuthApiService } from '@common/services/api/auth-api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { finalize } from 'rxjs/operators';
import { MyCookiesService } from '@common/services/my-cookies.service';

@Component({
  selector: 'app-authorization-page',
  templateUrl: './authorization-page.component.html',
  styleUrls: ['./authorization-page.component.scss']
})
export class AuthorizationPageComponent implements OnInit, OnDestroy {
  componentDestroyed$ = new Subject();

  form = new FormGroup({
    email: new FormControl('admin@admin.com', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('admin', [Validators.required, Validators.minLength(3)])
  });

  isLoading$ = new BehaviorSubject(false);

  constructor(private myCookiesService: MyCookiesService,
              private router: Router,
              private authApiService: AuthApiService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  submit(): void {
    if (this.form.valid) {
      this.setLoading(true);
      const model = {
        email: this.form.controls.email.value,
        password: this.form.controls.password.value
      };
      this.authApiService.login(model)
        .pipe(finalize(() => this.setLoading(false)))
        .subscribe(
          v => {
            if (v.result.authToken) {
              this.myCookiesService.put('token', v.result.authToken);
              this.router.navigate(['/']);
            }
          },
          () => this.openSnackBar());
    }
  }

  setLoading(flag: boolean): void {
    this.isLoading$.next(flag);
  }

  openSnackBar(): void {
    this.snackBar.open('Ошибка авторизации', '', {
      duration: 1000,
    });
  }
}
