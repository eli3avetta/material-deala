import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';
import { duplicatePassword } from '@common/utils/validations.utils';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.scss']
})
export class RegistrationPageComponent implements OnInit, OnDestroy {
  componentDestroyed$ = new Subject();

  form = new FormGroup({
    login: new FormControl('', [Validators.required, Validators.minLength(3)]),
    password: new FormControl('', [Validators.required, Validators.minLength(3)]),
    repeatPassword: new FormControl('')
  });

  optionsLogin = ['eli3avetta', 'kaktus3000', 'Иван'];
  optionsPassword = ['123', '111', '222'];
  filteredLogins: Observable<string[]>;
  filteredPasswords: Observable<string[]>;

  constructor() {
  }

  ngOnInit(): void {
    this.form.controls.repeatPassword.setValidators([duplicatePassword(this.form.controls.password)]);

    this.filteredLogins = this.form.controls.login.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        startWith(''),
        map(value => this._filter(value, this.optionsLogin))
      );

    this.filteredPasswords = this.form.controls.password.valueChanges
      .pipe(
        takeUntil(this.componentDestroyed$),
        startWith(''),
        map(value => this._filter(value, this.optionsPassword))
      );
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  _filter(value: any, options: string[]): string[] {
    value = value.toLowerCase();
    return options.filter(option => option.toLowerCase().indexOf(value) === 0);
  }

  submit(): void {
    //   if (this.form.valid) {
    //     this.sessionService.setUser({ login: this.form.controls.login.value });
    //     this.router.navigate(['/']);
    //   }
  }
}
