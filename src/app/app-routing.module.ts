import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsAuthGuard } from './common/guards/is-auth.guard';
import { IsNotAuthGuard } from './common/guards/is-not-auth.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./main/main.module')
      .then(m => m.MainModule),
    canActivate: [IsAuthGuard]
  },
  {
    path: 'not-auth',
    loadChildren: () => import('./not-auth/not-auth.module')
      .then(m => m.NotAuthModule),
    canActivate: [IsNotAuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule],
  providers: [
    IsAuthGuard,
    IsNotAuthGuard
  ]
})
export class AppRoutingModule { }
