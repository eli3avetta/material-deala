import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'inputs',
        loadChildren: () => import('./sections/inputs-section/inputs-section.module')
          .then(m => m.InputsSectionModule)
      },
      {
        path: 'forms',
        loadChildren: () => import('./sections/forms-section/forms-section.module')
            .then(m => m.FormsSectionModule)
      },
      {
        path: 'test',
        loadChildren: () => import('./sections/test-section/test-section.module')
            .then(m => m.TestSectionModule)
      },
      {
        path: 'deala',
        loadChildren: () => import('./sections/deala-section/deala-section.module')
            .then(m => m.DealaSectionModule)
      },
      {
        path: 'websocket',
        loadChildren: () => import('./sections/websocket-section/websocket-section.module')
          .then(m => m.WebsocketSectionModule)
      },
      {
        path: '',
        redirectTo: 'inputs/checkbox'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
