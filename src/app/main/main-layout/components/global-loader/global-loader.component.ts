import { Component, OnInit } from '@angular/core';
import { GlobalLoaderService } from '@common/services/global-loader.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-global-loader',
  templateUrl: './global-loader.component.html',
  styleUrls: ['./global-loader.component.scss']
})
export class GlobalLoaderComponent implements OnInit {
  isActive$ = this.globalLoaderService.isActive$;

  constructor(private globalLoaderService: GlobalLoaderService) {
  }

  ngOnInit(): void {
  }

}
