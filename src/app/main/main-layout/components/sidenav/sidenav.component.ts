import { Component, OnInit } from '@angular/core';
import { LayoutService } from '@common/services/layout.service';
import { SidenavItem } from '@common/interfaces/sidenav-item.interface';

@Component({
  selector: 'app-side-nave',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})

export class SidenavComponent implements OnInit {
  sections: SidenavItem[];

  constructor(private layoutService: LayoutService) {
  }

  ngOnInit(): void {
    this.sections = [
      {
        sectionName: 'Inputs', pages: [
          {
            pageName: 'checkbox',
            pageLink: 'inputs/checkbox'
          },
          {
            pageName: 'datepicker',
            pageLink: 'inputs/datepicker'
          },
          {
            pageName: 'datepicker range',
            pageLink: 'inputs/datepicker-range'
          },
          {
            pageName: 'chips',
            pageLink: 'inputs/chips'
          },
          {
            pageName: 'radio',
            pageLink: 'inputs/radio'
          },
          {
            pageName: 'select',
            pageLink: 'inputs/select'
          },
          {
            pageName: 'slider toggle',
            pageLink: 'inputs/slider-toggle'
          },
          {
            pageName: 'slider',
            pageLink: 'inputs/slider'
          }
        ]
      },
      {
        sectionName: 'Forms', pages: [
          {
            pageName: 'Заказ пиццы',
            pageLink: 'forms/ordering-pizza'
          }
        ]
      },
      {
        sectionName: 'Test', pages: [
          {
            pageName: 'Test',
            pageLink: 'test'
          }
        ]
      },
      {
        sectionName: 'Deala', pages: [
          {
            pageName: 'shops',
            pageLink: 'deala/shops'
          },
          {
            pageName: 'coupons',
            pageLink: 'deala/coupons'
          }
        ]
      },
      {
        sectionName: 'Websocket', pages: [
          {
            pageName: 'Echo Test',
            pageLink: 'websocket/echo-test'
          }
        ]
      }
    ];
  }

  sidenavHide(): void {
    this.layoutService.setSidenavShow(false);
  }
}
