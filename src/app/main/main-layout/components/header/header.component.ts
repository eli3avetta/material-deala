import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { LayoutService } from '@common/services/layout.service';
import { MyCookiesService } from '@common/services/my-cookies.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userName$ = new BehaviorSubject('Имя');
  sidenavIsShow$ = this.layoutService.sidenavIsShow$;

  constructor(private myCookiesService: MyCookiesService,
              private router: Router,
              private layoutService: LayoutService) {
  }

  ngOnInit(): void {
  }

  exit(): void {
    this.myCookiesService.remove('token');
  }

  toggleSidenav(): void {
    this.layoutService.setSidenavShow(true);
  }
}
