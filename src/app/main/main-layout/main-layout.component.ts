import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LayoutService } from '@common/services/layout.service';
import { IsBrowserService } from '@common/services/is-browser.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit, OnDestroy {
  componentDestroyed$ = new Subject();
  @ViewChild('drawerComponent', { static: true }) drawerComponent: MatDrawer;

  constructor(private layoutService: LayoutService,
              public isBrowserService: IsBrowserService) {
  }

  ngOnInit(): void {
    this.layoutService.sidenavIsShow$
      .pipe(takeUntil(this.componentDestroyed$))
      .subscribe((flag: boolean) => {
        if (flag) {
          this.drawerComponent.open();
        } else {
          this.drawerComponent.close();
        }
      });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }
}
