import { ChangeDetectionStrategy, Component, ContentChild, ElementRef, OnInit } from '@angular/core';
import { FormFieldComponent } from './pages/components/form-field/form-field.component';
import { BehaviorSubject } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-test-section',
  templateUrl: './test-section.component.html',
  styleUrls: ['./test-section.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestSectionComponent implements OnInit {

  form = this.fb.group({
    first: ['', [Validators.required]],
    second: ['', [Validators.required, Validators.minLength(3)]]
  });


  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
  }
}

