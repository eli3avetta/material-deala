import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  Input,
  OnInit,
  Renderer2
} from '@angular/core';
import { AbstractControl, FormControl, NgControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

const DEFAULT_ERRORS = {
  default: 'Некорректное значение',
  required: 'Обязетельно для заполнения'
};

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldComponent implements OnInit, AfterViewInit {
  @ContentChild('child', { static: true }) child: ElementRef;
  @ContentChild(NgControl) ngControl: NgControl;
  @Input() type: 'first' | 'second';
  @Input() errorMessages: { [key: string]: string };
  control: FormControl | AbstractControl;

  error$ = new BehaviorSubject(null);

  constructor(private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.renderer.addClass(this.child.nativeElement, 'gl-input-text');
    this.renderer.addClass(this.child.nativeElement, `mod-type-${ this.type }`);

    this.errorMessages = {
      ...DEFAULT_ERRORS,
      ...this.errorMessages
    };
  }

  ngAfterViewInit(): void {
    this.control = this.ngControl.control;
    this.child.nativeElement.onblur = () => {
      this.renderer.addClass(this.child.nativeElement, 'touched');
      this.defineError();
    };

    this.control.statusChanges.subscribe(() => this.defineError());
  }

  defineError(): void {
    if (this.control.errors) {
      this.renderer.addClass(this.child.nativeElement, 'invalid');

      if (this.control.touched) {
        Object.keys(this.control.errors).forEach(key => {
          this.error$.next(this.errorMessages[key] || this.errorMessages.default);
        });
      }
    } else {
      this.renderer.removeClass(this.child.nativeElement, 'invalid');
      this.error$.next(null);
    }
  }
}
