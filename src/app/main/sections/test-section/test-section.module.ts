import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { FormFieldComponent } from './pages/components/form-field/form-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestSectionComponent } from './test-section.component';

const routes: Routes = [
  {
    path: '',
    component: TestSectionComponent
  }
];

@NgModule({
  declarations: [
    TestSectionComponent,
    FormFieldComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class TestSectionModule {
}
