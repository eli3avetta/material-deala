import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { ShopsSearchResponse } from '../../../../../common/interfaces/api/shops-search-response.interface';
import { ShopsApiService } from '../../../../../common/services/api/shops-api.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ShopsSearchRequest } from '../../../../../common/interfaces/api/shops-search-request.interface';
import { LIMIT_PAGE } from './shops-list-page.utils';

@Injectable()
export class ShopsListPageResolve implements Resolve<ShopsSearchResponse> {
  constructor(private shopsApiService: ShopsApiService) {
  }

  resolve(): Observable<ShopsSearchResponse> {
    const model: ShopsSearchRequest = {
      limit: LIMIT_PAGE,
      offset: 0
    };
    return this.shopsApiService.searchShops(model)
        .pipe(
            catchError(() => of(null))
        );
  }
}
