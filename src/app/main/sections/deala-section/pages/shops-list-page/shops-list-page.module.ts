import { NgModule } from '@angular/core';
import { ShopsListPageComponent } from './shops-list-page.component';
import { ShopItemComponent } from './shop-item/shop-item.component';
import { RouterModule, Routes } from '@angular/router';
import { ShopsListPageResolve } from './shops-list-page.resolve';
import { CommonModule } from '@angular/common';
import { PipesModule } from '../../../../../common/pipes/pipes.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const routes: Routes = [
  {
    path: '',
    component: ShopsListPageComponent,
    resolve: {pageData: ShopsListPageResolve}
  }
];

@NgModule({
  declarations: [
    ShopsListPageComponent,
    ShopItemComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    PipesModule,
    InfiniteScrollModule
  ],
  providers: [
    ShopsListPageResolve
  ]
})
export class ShopsListPageModule {
}
