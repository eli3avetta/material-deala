import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShopsSearchResponse } from '../../../../../common/interfaces/api/shops-search-response.interface';
import { BehaviorSubject, Subject } from 'rxjs';
import { filter, finalize, skip, switchMap, takeUntil, tap } from 'rxjs/operators';
import { ShopsSearchRequest } from '../../../../../common/interfaces/api/shops-search-request.interface';
import { ShopsApiService } from '../../../../../common/services/api/shops-api.service';
import { ShopModelInterface } from '../../../../../common/interfaces/shop-model.interface';
import { LIMIT_PAGE } from './shops-list-page.utils';

@Component({
  selector: 'app-shops-page',
  templateUrl: './shops-list-page.component.html',
  styleUrls: ['./shops-list-page.component.scss']
})
export class ShopsListPageComponent implements OnInit, OnDestroy {
  componentDestroyed$ = new Subject();
  search$ = new Subject();

  shops$ = new BehaviorSubject<ShopModelInterface[]>(null);
  isLoading$ = new BehaviorSubject(false);
  isErrors$ = new BehaviorSubject(false);

  lastPage = false;

  constructor(private activatedRoute: ActivatedRoute,
              private shopsApiService: ShopsApiService) {
  }

  ngOnInit(): void {
    const pageData: ShopsSearchResponse = this.activatedRoute.snapshot.data.pageData;
    if (pageData) {
      this.parseData(pageData);
      this.initSearch();
    } else {
      this.isErrors$.next(true);
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  initSearch(): void {
    this.search$
      .pipe(
        takeUntil(this.componentDestroyed$),
        tap(() => this.setLoading(true)),
        switchMap(() => {
          const model: ShopsSearchRequest = {
            limit: LIMIT_PAGE,
            offset: this.shops$.value.length
          };
          return this.shopsApiService.searchShops(model)
            .pipe(finalize(() => this.setLoading(false)));
        })
      )
      .subscribe(
        (data: ShopsSearchResponse) => {
          this.parseData(data);
        },
        () => this.isErrors$.next(true));
  }

  parseData(data: ShopsSearchResponse): void {
    this.setShopsList(data.data);
    if (this.shops$.value.length >= data.totalItems) {
      this.lastPage = true;
    }
  }

  onScroll(): void {
    this.nextPage();
  }

  setShopsList(shops: ShopModelInterface[]): void {
    this.shops$.next(this.shops$.value ? [...this.shops$.value, ...shops] : shops);
  }

  nextPage(): void {
    if (!this.lastPage && !this.isLoading$.value) {
      this.search$.next();
    }
  }

  setLoading(flag): void {
    this.isLoading$.next(flag);
  }
}
