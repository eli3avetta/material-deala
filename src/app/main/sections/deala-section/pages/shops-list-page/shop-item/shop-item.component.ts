import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ShopModelInterface } from '../../../../../../common/interfaces/shop-model.interface';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss']
})
export class ShopItemComponent implements OnInit {
  @Input() shop: ShopModelInterface;

  shop$ = new BehaviorSubject(null);
  link$ = new BehaviorSubject<string>(null);
  title$ = new BehaviorSubject<string>(null);
  logo$ = new BehaviorSubject<string>(null);

  constructor() {
  }

  ngOnInit(): void {
    this.shop$.next(this.shop);

    if (this.shop.link) {
      this.link$.next(this.shop.link);
    }

    if (this.shop.title) {
      this.title$.next(this.shop.title);
    } else {
      this.title$.next('(нет названия)');
    }

    if (this.shop.logo) {
      this.logo$.next(this.shop.logo.previewUrl);
    } else {
      this.logo$.next('https://upload.wikimedia.org/wikipedia/commons/2/2c/.no_logo.svg');
    }
  }

}
