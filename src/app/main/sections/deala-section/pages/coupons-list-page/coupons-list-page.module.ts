import { NgModule } from '@angular/core';
import { CouponsListPageComponent } from './coupons-list-page.component';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './components/filter/filter.component';
import { TableComponent } from './components/table/table.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ColumnSizeDirectiveModule } from '../../../../../common/directives/column-size/column-size.directive.module';
import { CouponsListPageResolve } from './coupons-list-page.resolve';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PipesModule } from '../../../../../common/pipes/pipes.module';

const routes: Routes = [
  {
    path: '',
    component: CouponsListPageComponent,
    resolve: {pageData: CouponsListPageResolve}
  }
];

@NgModule({
  declarations: [
    CouponsListPageComponent,
    FilterComponent,
    TableComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatSelectModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    ColumnSizeDirectiveModule,
    InfiniteScrollModule,
    PipesModule,
    FormsModule
  ],
  providers: [
    CouponsListPageResolve
  ]
})
export class CouponsListPageModule {
}
