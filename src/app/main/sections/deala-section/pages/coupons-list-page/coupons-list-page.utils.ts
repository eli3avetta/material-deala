import { CouponSortingOrder, CouponSortingType } from '../../../../../common/enums/coupon-sorting.enum';

export const LIMIT_PAGE =  20;
export const SORTING_DEFAULT = [
  { type: CouponSortingType.Status, order: CouponSortingOrder.Asc },
  { type: CouponSortingType.CreatedDate, order: CouponSortingOrder.Desk }
];
