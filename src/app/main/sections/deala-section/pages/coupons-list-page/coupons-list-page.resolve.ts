import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { CouponsApiService } from '../../../../../common/services/api/coupons-api.service';
import { forkJoin, Observable, of } from 'rxjs';
import { CouponsSearchRequest } from '../../../../../common/interfaces/api/coupons-search-request.interface';
import { catchError, map } from 'rxjs/operators';
import { UsersApiService } from '../../../../../common/services/api/users-api.service';
import { UsersSearchRequest } from '../../../../../common/interfaces/api/users-search-request.interface';
import { ProgramsSearchRequest } from '../../../../../common/interfaces/api/programs-search-request.interface';
import { ProgramsApiService } from '../../../../../common/services/api/programs-api.service';
import { CouponsListResolveInterface } from '../../../../../common/interfaces/resolves/coupons-list-resolve.interface';
import { LIMIT_PAGE, SORTING_DEFAULT } from './coupons-list-page.utils';

@Injectable()
export class CouponsListPageResolve implements Resolve<CouponsListResolveInterface> {
  constructor(private couponsApiService: CouponsApiService,
              private usersApiService: UsersApiService,
              private programsApiService: ProgramsApiService) {
  }

  resolve(): Observable<CouponsListResolveInterface> {
    const today = new Date();
    const year = today.getFullYear();
    const month = today.getMonth();
    const day = today.getDate();
    const fromMS = new Date(year, month, 1).getTime();
    const toMS = new Date(year, month, day).getTime();
    const sorting = SORTING_DEFAULT;

    const modelCoupons: CouponsSearchRequest = {
      limit: LIMIT_PAGE,
      offset: 0,
      fromMS,
      toMS,
      sorting
    };

    const modelPrograms: ProgramsSearchRequest = {
      limit: 9999,
      sorting: [
        {
          order: 'ASC',
          type: 'PRIORITY_ORDER'
        }
      ]
    };

    const modelUsers: UsersSearchRequest = {
      limit: 9999,
      offset: 0,
      sorting: [
        {
          order: 'ASC',
          type: 'FIRST_NAME'
        }
      ]
    };

    return forkJoin([
      this.couponsApiService.searchCoupons(modelCoupons).pipe(catchError(() => of(null))),
      this.programsApiService.searchPrograms(modelPrograms).pipe(catchError(() => of(null))),
      this.usersApiService.searchUsers(modelUsers).pipe(catchError(() => of(null)))
    ])
      .pipe(
        catchError(() => of(null)),
        map(([couponsData, programsData, usersData]) => ({
          couponsData,
          programsData,
          usersData,
          fromMS,
          toMS
        }))
      );
  }
}
