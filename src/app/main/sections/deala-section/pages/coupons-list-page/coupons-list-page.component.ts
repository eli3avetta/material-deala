import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { CouponsSearchResponse } from '../../../../../common/interfaces/api/coupons-search-response.interface';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { ProgramsSearchResponse } from '../../../../../common/interfaces/api/programs-search-response.interface';
import { UsersSearchResponse } from '../../../../../common/interfaces/api/users-search-response.interface';
import { CouponsApiService } from '../../../../../common/services/api/coupons-api.service';
import { debounceTime, distinctUntilChanged, finalize, switchMap, takeUntil, tap } from 'rxjs/operators';
import { CouponsSearchRequest } from '../../../../../common/interfaces/api/coupons-search-request.interface';
import { LIMIT_PAGE } from '../shops-list-page/shops-list-page.utils';
import { TableComponent } from './components/table/table.component';
import { cleanObject } from '../../../../../common/utils/common.utils';
import { FormControl, FormGroup } from '@angular/forms';
import { CouponsListResolveInterface } from '../../../../../common/interfaces/resolves/coupons-list-resolve.interface';
import { FilterComponent } from './components/filter/filter.component';

@Component({
  selector: 'app-coupons-list-page',
  templateUrl: './coupons-list-page.component.html',
  styleUrls: ['./coupons-list-page.component.scss']
})
export class CouponsListPageComponent implements OnInit, OnDestroy {
  @ViewChild('tableComponent', { static: true }) tableComponent: TableComponent;
  @ViewChild('filterComponent', { static: true }) filterComponent: FilterComponent;

  formFilter = new FormGroup({
    query: new FormControl(null),
    fromMS: new FormControl(null),
    toMS: new FormControl(null),
    affiliateProgramIds: new FormControl(null),
    withAffiliatePrograms: new FormControl(null),
    creatorId: new FormControl(null),
    status: new FormControl(null),
    sorting: new FormControl(null),
  });

  componentDestroyed$ = new Subject();
  search$ = new Subject();

  coupons$ = new BehaviorSubject([]);
  isLoading$ = new BehaviorSubject(false);
  isError$ = new BehaviorSubject(false);

  lastPage = false;

  programsData: ProgramsSearchResponse;
  usersData: UsersSearchResponse;

  constructor(private activatedRoute: ActivatedRoute,
              private couponsApiService: CouponsApiService) {
  }

  ngOnInit(): void {
    const pageData: CouponsListResolveInterface = this.activatedRoute.snapshot.data.pageData;
    if (pageData) {
      this.programsData = pageData.programsData;
      this.usersData = pageData.usersData;

      this.parseData(pageData.couponsData);

      this.filterComponent.changeData(pageData.fromMS, pageData.toMS)

      this.initSearch();

      Object.keys(this.formFilter.controls).forEach(key => {
        const control = this.formFilter.controls[key];
        control.valueChanges
          .pipe(
            takeUntil(this.componentDestroyed$),
            distinctUntilChanged()
          )
          .subscribe(() => this.search());
      });
    } else {
      this.isError$.next(true);
    }
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  initSearch(): void {
    this.search$
      .pipe(
        debounceTime(300),
        takeUntil(this.componentDestroyed$),
        tap(isNextPage => {
          if (!isNextPage) {
            this.tableComponent.setCouponsList(null);
          }
          this.setLoading(true);
        }),
        switchMap(() => {
          const model: CouponsSearchRequest = {
            limit: LIMIT_PAGE,
            offset: this.coupons$.value.length,
            ...this.formFilter.value
          };
          return this.couponsApiService.searchCoupons(cleanObject(model))
            .pipe(
              finalize(() => this.setLoading(false))
            );
        })
      )
      .subscribe((data: CouponsSearchResponse) => {
          this.parseData(data);
        },
        () => this.isError$.next(true));
  }

  parseData(data: CouponsSearchResponse): void {
    this.filterComponent.changeCouponsNum(data.totalItems);
    this.tableComponent.setCouponsList(data.data);
    if (this.coupons$.value.length >= data.totalItems) {
      this.lastPage = true;
    }
  }

  setLoading(flag): void {
    this.isLoading$.next(flag);
  }

  nextPage(): void {
    if (!this.lastPage && !this.isLoading$.value) {
      this.search(true);
    }
  }

  search(newPage?: boolean): void {
    this.search$.next(newPage);
  }
}
