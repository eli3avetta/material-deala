import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { ProgramsSearchResponse } from '../../../../../../../common/interfaces/api/programs-search-response.interface';
import { UsersSearchResponse } from '../../../../../../../common/interfaces/api/users-search-response.interface';
import { ProgramModelInterface } from '../../../../../../../common/interfaces/api/program-model.interface';
import { UserModelInterface } from '../../../../../../../common/interfaces/user-model.interface';
import { CouponStatusesEnum } from '../../../../../../../common/enums/coupon-statuses.enum';

const VALUE_ALL = 'All';
const PROGRAMS_VALUE_WITHOUT = 'Without program';

const STATUS_ITEMS_LIST = [
  { statusName: null, value: VALUE_ALL },
  { statusName: CouponStatusesEnum.Active, value: 'Active' },
  { statusName: CouponStatusesEnum.Inactive, value: 'Inactive' },
  { statusName: CouponStatusesEnum.Rejected, value: 'Rejected' },
  { statusName: CouponStatusesEnum.Moderation, value: 'Moderation' }
];

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit, OnDestroy {
  @Input() programsData: ProgramsSearchResponse;
  @Input() usersData: UsersSearchResponse;
  @Input() formFilter: FormGroup;

  fromDate = new FormControl('');
  toDate = new FormControl('');
  selectedAffiliatePrograms = new FormControl([VALUE_ALL]);
  creatorId = new FormControl(VALUE_ALL);
  status = new FormControl(VALUE_ALL);

  componentDestroyed$ = new Subject();

  programsItemsList$ = new BehaviorSubject([]);
  usersItemsList$ = new BehaviorSubject([]);
  statusItemsList$ = new BehaviorSubject(STATUS_ITEMS_LIST);
  couponsNum$ = new BehaviorSubject(null);

  constructor() {
  }

  ngOnInit(): void {
    this.setProgramsList(this.programsData.data);
    this.setUsersList(this.usersData.data);
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  changeFromMS(e): void {
    this.formFilter.controls.fromMS.setValue(e.getTime());
  }

  changeToMS(e): void {
    this.formFilter.controls.toMS.setValue(e.getTime());
  }

  setProgramsList(programs: ProgramModelInterface[]): void {
    this.programsItemsList$.next([
      { id: null, programName: VALUE_ALL },
      { id: null, programName: PROGRAMS_VALUE_WITHOUT },
      ...programs.map(program => ({ id: program.id, programName: program.title }))
    ]);
  }

  setUsersList(users: UserModelInterface[]): void {
    this.usersItemsList$.next([
      { id: null, value: VALUE_ALL },
      ...users.map(user => ({ id: user.id, value: `${ user.firstName } ${ user.lastName }` }))
    ]);
  }

  selectProgram(item): void {
    const isAdd = this.selectedAffiliatePrograms.value.includes(item.programName);

    switch (true) {
      case item.programName === VALUE_ALL:
      case item.programName === PROGRAMS_VALUE_WITHOUT && !isAdd:
        this.selectedAffiliatePrograms.setValue([VALUE_ALL]);
        this.formFilter.controls.withAffiliatePrograms.setValue(null);
        this.formFilter.controls.affiliateProgramIds.setValue(null);
        break;

      case item.programName === PROGRAMS_VALUE_WITHOUT && isAdd:
        this.selectedAffiliatePrograms.setValue([PROGRAMS_VALUE_WITHOUT]);
        this.formFilter.controls.withAffiliatePrograms.setValue(false);
        this.formFilter.controls.affiliateProgramIds.setValue(null);
        break;

      case item.programName !== VALUE_ALL && item.programName !== PROGRAMS_VALUE_WITHOUT:
        const newSelected = this.selectedAffiliatePrograms.value
          .filter(programName => programName !== VALUE_ALL && programName !== PROGRAMS_VALUE_WITHOUT);
        this.selectedAffiliatePrograms.setValue(newSelected);
        const newValue = newSelected.map(
          programName => this.programsData.data.find(program => program.title === programName).id
        );
        this.formFilter.controls.withAffiliatePrograms.setValue(null);
        this.formFilter.controls.affiliateProgramIds.setValue(newValue);
    }
  }

  changeData(fromMS, toMS): void {
    this.fromDate.setValue(fromMS ? new Date(fromMS) : null);
    this.toDate.setValue(toMS ? new Date(toMS) : null);
  }

  changeCreatorId(item): void {
    this.formFilter.controls.creatorId.setValue(item);
  }

  changeStatus(item): void {
    this.formFilter.controls.status.setValue(item);
  }

  changeCouponsNum(num): void {
    this.couponsNum$.next(num);
    console.log(this.status.value);
  }
}
