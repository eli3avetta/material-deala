import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CouponModelInterface } from '../../../../../../../common/interfaces/coupon-model.interface';
import { calculatedColumnsWidth } from '../../../../../../../common/utils/table.utils';
import { CouponsSearchRequestSorting } from '../../../../../../../common/interfaces/api/coupons-search-request.interface';
import { CouponSortingType } from '../../../../../../../common/enums/coupon-sorting.enum';
import { FormGroup } from '@angular/forms';
import { SORTING_DEFAULT } from '../../coupons-list-page.utils';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnDestroy {
  @Input() formFilter: FormGroup;

  displayedColumns = ['code', 'description', 'clicks', 'rating', 'merchant', 'dateAdded', 'source', 'creator', 'status'];
  dataSource = new MatTableDataSource<CouponModelInterface>([]);

  COLUMN_NAME = {
    code: 'code',
    description: 'description',
    clicks: 'clicks',
    rating: 'rating',
    merchant: 'merchant',
    dateAdded: 'dateAdded',
    source: 'source',
    creator: 'creator',
    status: 'status'
  };

  COLUMN_SIZE = calculatedColumnsWidth({
    [this.COLUMN_NAME.code]: 4,
    [this.COLUMN_NAME.description]: 6,
    [this.COLUMN_NAME.clicks]: 3,
    [this.COLUMN_NAME.rating]: 3,
    [this.COLUMN_NAME.merchant]: 4,
    [this.COLUMN_NAME.dateAdded]: 5,
    [this.COLUMN_NAME.source]: 4,
    [this.COLUMN_NAME.creator]: 4,
    [this.COLUMN_NAME.status]: 3,
  });

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }

  setCouponsList(coupons: CouponModelInterface[]): void {
    this.dataSource.data = coupons ? [...this.dataSource.data, ...coupons] : [];
  }

  sort(e): void {
    if (e.direction) {
      const newSort: CouponsSearchRequestSorting = { order: null, type: null };
      newSort.order = e.direction.toUpperCase();

      switch (e.active) {
        case this.COLUMN_NAME.clicks:
          newSort.type = CouponSortingType.ClicksCount;
          break;
        case this.COLUMN_NAME.rating:
          newSort.type = CouponSortingType.SuccessRate;
          break;
        case this.COLUMN_NAME.dateAdded:
          newSort.type = CouponSortingType.CreatedDate;
          break;
      }

      this.formFilter.controls.sorting.setValue([newSort]);
    } else {
      this.formFilter.controls.sorting.setValue(SORTING_DEFAULT);
    }
  }
}
