import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DealaSectionComponent } from './deala-section.component';

const routes: Routes = [
  {
    path: '',
    component: DealaSectionComponent,
    children: [
      {
        path: 'shops',
        loadChildren: () => import('./pages/shops-list-page/shops-list-page.module')
          .then(m => m.ShopsListPageModule)
      },
      {
        path: 'coupons',
        loadChildren: () => import('./pages/coupons-list-page/coupons-list-page.module')
          .then(m => m.CouponsListPageModule)
      }
    ]
  }
];

@NgModule({
  declarations: [
    DealaSectionComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DealaSectionModule {
}
