import { Component, OnInit } from '@angular/core';
import { Task } from '@common/interfaces/task-checkbox.interface';

@Component({
  selector: 'app-main-page',
  templateUrl: './checkbox-page.component.html',
  styleUrls: ['./checkbox-page.component.scss']
})
export class CheckboxPageComponent implements OnInit {
  checkboxValue = [];

  task: Task = {
    id: 1,
    title: 'Большая задача',
    completed: false,
    color: 'primary',
    main: true,
    subtasks: [
      { id: 2, title: 'Первое задание', completed: false, color: 'primary' },
      { id: 3, title: 'Второе задание', completed: false, color: 'accent' },
      { id: 4, title: 'Третье задание', completed: false, color: 'warn' }
    ]
  };

  allComplete = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  updateAllComplete(): void {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }

  setAll(completed: boolean): void {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => t.completed = completed);
  }

  checkTask(task: Task): void {
    if (task.main) {
      this.checkboxValue = task.completed ? task.subtasks.map(t => t.id) : [];
    } else {
      if (this.checkboxValue.includes(task.id)) {
        const index = this.checkboxValue.indexOf(task.id);
        this.checkboxValue.splice(index, 1);
      } else {
        this.checkboxValue.push(task.id);
      }
    }
  }

  next(): void {
    console.log(this.checkboxValue);
  }
}
