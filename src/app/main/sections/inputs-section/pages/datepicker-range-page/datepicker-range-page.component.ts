import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-datepicker-range-page',
  templateUrl: './datepicker-range-page.component.html',
  styleUrls: ['./datepicker-range-page.component.scss']
})
export class DatepickerRangePageComponent implements OnInit {
  campaignOne: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();

    this.campaignOne = new FormGroup({
      start: new FormControl(new Date(year, month, 1)),
      end: new FormControl(new Date(year, month, 6))
    });
  }

  submit(): void {
    const startDate = this.campaignOne.controls.start.value;
    const endDate = this.campaignOne.controls.end.value;
    const datepickerRange = {
      startDate: `${ startDate.getMonth() + 1 }/${ startDate.getDate() }/${ startDate.getFullYear() }`,
      endDate: `${ endDate.getMonth() + 1 }/${ endDate.getDate() }/${ endDate.getFullYear() }`,
    };
    console.log(datepickerRange);
  }
}
