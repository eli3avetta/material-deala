import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-datepicker-page',
  templateUrl: './datepicker-page.component.html',
  styleUrls: ['./datepicker-page.component.scss']
})
export class DatepickerPageComponent implements OnInit {
  @ViewChild('date', { static: false }) date: ElementRef;
  currentDate = new Date();
  dateControl = new FormControl(new Date(), [Validators.required]);

  constructor() {
  }

  ngOnInit(): void {
    this.getDefaultDate();
  }

  getDefaultDate(): void {
    const year = this.currentDate.getFullYear();
    const month: number = this.currentDate.getMonth();
    const day: number = this.currentDate.getDate();
    this.dateControl.setValue(new Date(year, month, day));
  }

  submit(): void {
    if (this.date.nativeElement.value) {
      this.dateControl.setValue(this.date.nativeElement.value);
      this.dateControl.markAsUntouched();
    }
    console.log(this.dateControl.value);
  }
}
