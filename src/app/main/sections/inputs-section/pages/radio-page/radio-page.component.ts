import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-radio-page',
  templateUrl: './radio-page.component.html',
  styleUrls: ['./radio-page.component.scss']
})
export class RadioPageComponent implements OnInit, OnDestroy {
  radioControl = new FormControl('');
  items$ = new BehaviorSubject('');
  componentDestroyed$ = new Subject();
  seasons: string[] = ['Зима', 'Весна', 'Лето', 'Осень'];

  constructor() {
  }

  ngOnInit(): void {
    this.radioControl.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(value => this.items$.next(value));
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  submit(): void {
    console.log(this.radioControl.value);
  }
}
