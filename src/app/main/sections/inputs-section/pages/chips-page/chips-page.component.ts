import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Chip } from '../../../../../common/utils/chip.interface';

@Component({
  selector: 'app-chips-page',
  templateUrl: './chips-page.component.html',
  styleUrls: ['./chips-page.component.scss']
})
export class ChipsPageComponent {
  selectable = true;
  removable = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  chipsControl = new FormControl([]);
  chips$ = new BehaviorSubject<Chip[]>([
    { name: 'Начальное значение' }
  ]);

  constructor() {
  }

  add(): void {
    if (this.chipsControl.value) {
      this.chips$.next([...this.chips$.value, { name: this.chipsControl.value }]);
    }
    this.chipsControl.setValue('');
  }

  remove(item: Chip): void {
    const index = this.chips$.value.indexOf(item);

    if (index > -1) {
      this.chips$.next(this.chips$.value.splice(index, 1));
    }
  }

  submit(): void {
    const result = this.chips$.value.map(item => item.name);
    console.log(result);
    this.resetItems();
  }

  resetItems(): void {
    this.chipsControl.setValue('');
    this.chips$.next([]);
  }
}
