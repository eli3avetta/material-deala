import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-select-page',
  templateUrl: './select-page.component.html',
  styleUrls: ['./select-page.component.scss']
})
export class SelectPageComponent implements OnInit, OnDestroy {
  selectControl = new FormControl();
  toppingList: string[] = ['сыр', 'грибы', 'лук', 'пепперони', 'перец', 'помидор'];
  items$ = new BehaviorSubject('');
  componentDestroyed$ = new Subject();

  constructor() {
  }

  ngOnInit(): void {
    this.selectControl.valueChanges
        .pipe(
            takeUntil(this.componentDestroyed$)
        )
        .subscribe(value => this.items$.next(value));
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  submit(): void {
    console.log(this.selectControl.value);
  }
}
