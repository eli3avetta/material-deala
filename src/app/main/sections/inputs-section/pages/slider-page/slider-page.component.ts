import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-slider-page',
  templateUrl: './slider-page.component.html',
  styleUrls: ['./slider-page.component.scss']
})
export class SliderPageComponent implements OnInit, OnDestroy {
  form = new FormGroup({
    min: new FormControl(),
    max: new FormControl(),
    price: new FormControl(),
    priceSlider: new FormControl()
  });

  min$ = new BehaviorSubject<number>(null);
  max$ = new BehaviorSubject<number>(null);
  price$ = new BehaviorSubject<number>(0);
  priceSlider$ = new BehaviorSubject<string>('');

  step$ = new BehaviorSubject<number>(null);

  componentDestroyed$ = new Subject();

  ngOnInit(): void {
    this.form.controls.min.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => this.min$.next(data));
    this.form.controls.min.setValue(0);

    this.form.controls.max.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => {
          this.max$.next(data);
          if (data <= 100) {
            this.step$.next(10);
          } else if (data > 100 && data <= 1000) {
            this.step$.next(10);
          } else {
            this.step$.next(100);
          }
        });
    this.form.controls.max.setValue(100);

    this.form.controls.price.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => {
          this.price$.next(data);
          this.priceSlider$.next(data);
        });

    this.form.controls.priceSlider.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => {
          this.priceSlider$.next(data);
          this.price$.next(data);
        });
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  submit(): void {
    if (this.form.controls.price.value) {
      console.log(this.form.controls.price.value);
    } else {
      console.log(this.form.controls.priceSlider.value);
    }
  }
}
