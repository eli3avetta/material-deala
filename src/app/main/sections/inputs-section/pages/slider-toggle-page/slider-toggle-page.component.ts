import { Component, OnDestroy, OnInit } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-slider-toggle-page',
  templateUrl: './slider-toggle-page.component.html',
  styleUrls: ['./slider-toggle-page.component.scss']
})
export class SliderTogglePageComponent implements OnInit, OnDestroy {
  form = new FormGroup({
    color: new FormControl(),
    checked: new FormControl(),
    disabled: new FormControl(),
  });

  color$ = new BehaviorSubject<ThemePalette>('accent');
  checked$ = new BehaviorSubject<boolean>(false);
  disabled$ = new BehaviorSubject<boolean>(false);

  componentDestroyed$ = new Subject();

  constructor() {
  }

  ngOnInit(): void {
    this.form.controls.color.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => this.color$.next(data));

    this.form.controls.checked.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => this.checked$.next(data));

    this.form.controls.disabled.valueChanges
        .pipe(takeUntil(this.componentDestroyed$))
        .subscribe(data => this.disabled$.next(data));
  }

  ngOnDestroy(): void {
    this.componentDestroyed$.next();
    this.componentDestroyed$.complete();
  }

  submit(): void {
    console.log(this.form.value);
  }
}
