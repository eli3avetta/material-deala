import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InputsSectionComponent } from './inputs-section.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckboxPageComponent } from './pages/checkbox-page/checkbox-page.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { DatepickerPageComponent } from './pages/datepicker-page/datepicker-page.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { ChipsPageComponent } from './pages/chips-page/chips-page.component';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { DatepickerRangePageComponent } from './pages/datepicker-range-page/datepicker-range-page.component';
import { RadioPageComponent } from './pages/radio-page/radio-page.component';
import { MatRadioModule } from '@angular/material/radio';
import { SelectPageComponent } from './pages/select-page/select-page.component';
import { MatSelectModule } from '@angular/material/select';
import { SliderTogglePageComponent } from './pages/slider-toggle-page/slider-toggle-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SliderPageComponent } from './pages/slider-page/slider-page.component';
import { MatSliderModule } from '@angular/material/slider';

const routes: Routes = [
  {
    path: '',
    component: InputsSectionComponent,
    children: [
      {
        path: 'checkbox',
        component: CheckboxPageComponent
      },
      {
        path: 'datepicker',
        component: DatepickerPageComponent
      },
      {
        path: 'datepicker-range',
        component: DatepickerRangePageComponent
      },
      {
        path: 'chips',
        component: ChipsPageComponent
      },
      {
        path: 'radio',
        component: RadioPageComponent
      },
      {
        path: 'select',
        component: SelectPageComponent
      },
      {
        path: 'slider-toggle',
        component: SliderTogglePageComponent
      },
      {
        path: 'slider',
        component: SliderPageComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    InputsSectionComponent,
    CheckboxPageComponent,
    DatepickerPageComponent,
    ChipsPageComponent,
    DatepickerRangePageComponent,
    RadioPageComponent,
    SelectPageComponent,
    SliderTogglePageComponent,
    SliderPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatCheckboxModule,
    FormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatIconModule,
    MatAutocompleteModule,
    MatChipsModule,
    MatRadioModule,
    MatSelectModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSliderModule
  ]
})
export class InputsSectionModule {
}
