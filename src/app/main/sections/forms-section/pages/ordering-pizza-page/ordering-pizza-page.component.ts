import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { ItemOrderInterface } from '@common/interfaces/item-order.interface';
import { someTrue } from '@common/utils/validations.utils';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ItemDateTimeInterface } from '@common/interfaces/item-date-time.interface';
import { DistrictsInterface } from '@common/interfaces/districts.interface';
import { DAY_IN_MS, MS_IN_MIN } from '@common/utils/consts';
import { IsBrowserService } from '@common/services/is-browser.service';

const DISTRICTS = [
  {
    id: 1,
    name: 'Авиастроительный'
  },
  {
    id: 2,
    name: 'Вахитовский'
  },
  {
    id: 3,
    name: 'Кировский'
  },
  {
    id: 4,
    name: 'Московский'
  },
  {
    id: 5,
    name: 'Ново-Савиновский'
  },
  {
    id: 6,
    name: 'Приволжский'
  },
  {
    id: 7,
    name: 'Советский'
  }
];

const ITEMS_ORDER_PIZZAS = [
  {
    id: 1,
    title: 'Чизбургер-пицца',
    text: 'Мясной соус болоньезе, моцарелла, красный лук, соленые огурчики, томаты, соус бургер',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/1959b0fdf5f049fb9ec12cf05d535bc7_292x292.jpeg',
    prise: 250,
    calories: 207
  },
  {
    id: 2,
    title: 'Аррива',
    text: 'Соус бургер, цыпленок, соус ранч, чоризо, сладкий перец, красный лук, моцарелла, томаты, чеснок',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/c4f196c92ac943d59047267af46ceb5d_292x292.jpeg',
    prise: 420,
    calories: 205
  },
  {
    id: 3,
    title: 'Гавайская',
    text: 'Цыпленок, томатный соус, моцарелла, ананасы',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/b952eb17-77b8-4a14-b982-42fbf5ceaf0e.jpg',
    prise: 900,
    calories: 176
  },
  {
    id: 4,
    title: 'Супермясная',
    text: 'Цыпленок, митболы из говядины, пикантная пепперони, томатный соус, острая чоризо, моцарелла, бекон',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/f7011a11-9c35-4428-a68a-d0c51ac3e538.jpg',
    prise: 340,
    calories: 280
  },
  {
    id: 5,
    title: 'Кисло-сладкий цыпленок',
    text: 'Цыпленок, томатный соус, моцарелла, кисло-сладкий соус',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/dcc14f3c-0bcd-4e22-9c94-d694750a790b.jpg',
    prise: 510,
    calories: 232
  },
  {
    id: 6,
    title: 'Цезарь',
    text: 'Свежие листья салата айсберг в конверте, цыплёнок, томаты черри, сыры чеддер и пармезан, моцарелла, сливочный соус, соус цезарь',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/4516c60c26ea4684ae2ed9a520b51906_292x292.jpeg',
    prise: 630,
    calories: 210
  }
];

const ITEMS_ORDER_DRINKS = [
  {
    id: 1,
    title: 'Coca-cola Orange',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/303941c2429242b2bb335601ec207798_292x292.jpeg',
    prise: 95,
    calories: 1
  },
  {
    id: 2,
    title: 'Sprite',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/Drinks/ru-RU/c674c8ff-638a-4ef5-a33a-0abe6a5dc92f.jpg',
    prise: 95,
    calories: 39
  },
  {
    id: 3,
    title: 'Schweppes bitter lemon',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/2d3500d8b5154f4cb394e046f40231f1_292x292.jpeg',
    prise: 75,
    calories: 34
  },
  {
    id: 4,
    title: 'Сок Rich Яблоко',
    imageLink: 'https://cdn.dodostatic.net/static/Img/Products/Drinks/ru-RU/a5e0a22c-217c-4d98-b8da-e8e10e8051d0.jpg',
    prise: 145,
    calories: 48
  }
];

const ITEMS_DATE_TIME: ItemDateTimeInterface[] = [];
for (let i = 8; i <= 23; i++) {
  ITEMS_DATE_TIME.push({
    hours: i,
    minutes: 0,
    text: `${ i }:00`
  });
  ITEMS_DATE_TIME.push({
    hours: i,
    minutes: 30,
    text: `${ i }:30`
  });
}

@Component({
  selector: 'app-ordering-pizza-page',
  templateUrl: './ordering-pizza-page.component.html',
  styleUrls: ['./ordering-pizza-page.component.scss']
})
export class OrderingPizzaPageComponent implements OnInit, OnDestroy {
  phoneReg = new RegExp(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/);
  @ViewChild('test', { static: true }) test: ElementRef;

  form = new FormGroup({
    userName: new FormControl('', [Validators.required, Validators.minLength(3)]),
    phone: new FormControl('', [Validators.required, Validators.pattern(this.phoneReg)]),
    orderReceipt: new FormControl('', [Validators.required]),
    district: new FormControl('', [Validators.required]),
    minPrise: new FormControl(800),
    maxPrise: new FormControl(1500),
    orderPizzasList: new FormControl('', [Validators.required]),
    orderDrinksList: new FormArray([], [someTrue()]),
    dateDay: new FormControl(new Date(), [Validators.required]),
    dateTime: new FormControl('', [Validators.required])
  });

  districts$ = new BehaviorSubject<DistrictsInterface[]>(DISTRICTS);
  itemsOrderPizzas$ = new BehaviorSubject<ItemOrderInterface[]>(ITEMS_ORDER_PIZZAS);
  itemsOrderDrinks$ = new BehaviorSubject<ItemOrderInterface[]>(ITEMS_ORDER_DRINKS);
  ordersListArr$ = new BehaviorSubject([]);

  itemsDateTime$ = new BehaviorSubject(ITEMS_DATE_TIME);
  filterDateDay;

  updateCurrentDate;
  currentDate$ = new BehaviorSubject(new Date());
  currentHours$ = new BehaviorSubject<number>(this.currentDate$.value.getHours());
  currentMinutes$ = new BehaviorSubject<number>(this.currentDate$.value.getMinutes());

  constructor(private snackBar: MatSnackBar,
              public isBrowserService: IsBrowserService) {
  }

  ngOnInit(): void {
    const formArray = this.form.controls.orderDrinksList as FormArray;
    this.itemsOrderDrinks$.value.forEach(() => {
      formArray.push(new FormControl(false));
    });


    if (this.isBrowserService.isBrowser) {
      this.updateCurrentDate = setInterval(() => {
        this.currentDate$.next(new Date());
        this.currentHours$.next(this.currentDate$.value.getHours());
        this.currentMinutes$.next(this.currentDate$.value.getMinutes());
      }, MS_IN_MIN);
    }

    this.filterDateDay = (d: Date | null): boolean => {
      const currentDate = new Date();
      const dateMsTimezone = new Date(currentDate.getTime() + currentDate.getTimezoneOffset() * MS_IN_MIN).getTime();
      const calendarDateMsTimezone = d.getTime() + (currentDate.getTime() % DAY_IN_MS);
      const availableOffsetMs = DAY_IN_MS * 3;
      return calendarDateMsTimezone >= dateMsTimezone && calendarDateMsTimezone <= dateMsTimezone + availableOffsetMs;
    };

    this.setDefaultDateDay();
  }

  ngOnDestroy(): void {
    clearInterval(this.updateCurrentDate);
  }

  inputOrderPizza(event, item): void {
    if (event.target.checked) {
      this.ordersListArr$.next([...this.ordersListArr$.value, item]);
    } else {
      const indexValue = this.ordersListArr$.value.indexOf(item);
      this.ordersListArr$.value.splice(indexValue, 1);
    }
    const valueArr = [];
    this.ordersListArr$.value.forEach(elem => {
      valueArr.push(elem.id);
    });
    this.form.controls.orderPizzasList.setValue(valueArr);
  }

  removeDrink(index): void {
    const formArray = this.form.controls.orderDrinksList as FormArray;
    formArray.controls[index].setValue(false);
  }

  setDefaultDateDay(): void {
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month: number = currentDate.getMonth();
    const day: number = currentDate.getDate();
    this.form.controls.dateDay.setValue(new Date(year, month, day));
  }

  submit(): void {
    this.form.markAllAsTouched();

    if (this.form.valid) {
      const model = this.form.value;
      model.orderDrinksList = model.orderDrinksList
        .map((flag, i) => flag ? this.itemsOrderDrinks$.value[i].id : 0)
        .filter(val => val);

      model.dateDay = {
        day: this.form.controls.dateDay.value.getDate(),
        month: this.form.controls.dateDay.value.getMonth() + 1,
        year: this.form.controls.dateDay.value.getFullYear()
      };
      this.openSnackBar();
      console.log(model);
    }
  }

  openSnackBar(): void {
    this.snackBar.open('Заказ оформлен 🍕', '', {
      duration: 2000,
    });
  }
}
