import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsSectionComponent } from './forms-section.component';
import { OrderingPizzaPageComponent } from './pages/ordering-pizza-page/ordering-pizza-page.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatDividerModule } from '@angular/material/divider';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { DisableControlDirectiveModule } from '@common/directives/disable-control/disable-control.directive.module';

const routes: Routes = [
  {
    path: '',
    component: FormsSectionComponent,
    children: [
      {
        path: 'ordering-pizza',
        component: OrderingPizzaPageComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    FormsSectionComponent,
    OrderingPizzaPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatRadioModule,
    MatDividerModule,
    MatSelectModule,
    DisableControlDirectiveModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTooltipModule,
    DisableControlDirectiveModule
  ]
})
export class FormsSectionModule {
}
