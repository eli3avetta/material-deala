import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WebsocketSectionComponent } from './websocket-section.component';
import { EchoTestPageComponent } from './pages/echo-test-page/echo-test-page.component';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: WebsocketSectionComponent,
    children: [
      {
        path: 'echo-test',
        component: EchoTestPageComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    WebsocketSectionComponent,
    EchoTestPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule
  ]
})
export class WebsocketSectionModule {
}
