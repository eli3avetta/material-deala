import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { WebsocketService } from '../../../../../common/services/websockets/websocket.service';

interface Log {
  status: string;
  value?: string;
  color?: string;
}

@Component({
  selector: 'app-echo-test-page',
  templateUrl: './echo-test-page.component.html',
  styleUrls: ['./echo-test-page.component.scss']
})
export class EchoTestPageComponent implements OnInit {
  @ViewChild('logWrapper', {static: true}) logWrapper: ElementRef;

  inputLocation = new FormControl('wss://echo.websocket.org');
  inputMessage = new FormControl('Сообщение');

  logs$ = new BehaviorSubject<Log[]>([]);

  isConnected$ = this.websocketService.isConnected$;
  isConnecting$ = this.websocketService.isConnecting$;

  logHeightScroll$ = new BehaviorSubject(null);

  constructor(private websocketService: WebsocketService) {
  }

  ngOnInit(): void {
    this.websocketService.isOpen$
      .subscribe(() => {
        this.addLog('CONNECTED', null, null);
      });

    this.websocketService.isMessage$
      .subscribe((event: MessageEvent) => {
        this.addLog('RESPONSE', event.data, 'green');
      });

    this.websocketService.isClose$
      .subscribe(() => {
        this.addLog('DISCONNECTED', null, null);
      });

    this.websocketService.isError$
      .subscribe((event: any) => {
        this.addLog('ERROR', event.data, 'red');
      });
  }

  createWebsocket(): void {
    const url = this.inputLocation.value;
    if (!this.isConnected$.value && !this.isConnecting$.value && url) {
      this.websocketService.init(url);
    }
  }

  clearWebsocket(): void {
    if (this.isConnecting$.value) {
      this.websocketService.close();
    }
  }

  sendMessage(): void {
    const message = this.inputMessage.value;
    if (this.isConnecting$.value) {
      this.websocketService.send(message);
      this.addLog('SENT', message, 'blur');
    }
  }

  addLog(status, value, color): void {
    const log: Log = { status, value, color };
    this.logs$.next([...this.logs$.value, log]);

    setTimeout(() => {
      this.logHeightScroll$.next(this.logWrapper.nativeElement.scrollHeight);
    });
  }

  clearLog(): void {
    this.logs$.next([]);
  }
}
