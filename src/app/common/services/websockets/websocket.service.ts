import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class WebsocketService {
  websocket;
  pingSetInterval;

  isOpen$ = new Subject();
  isMessage$ = new Subject();
  isClose$ = new Subject();
  isError$ = new Subject();

  isConnected$ = new BehaviorSubject(false);
  isConnecting$ = new BehaviorSubject(false);

  constructor() {
  }

  init(url): void {
    this.websocket = new WebSocket(url, []);
    this.pingSetInterval = setInterval(() => {
      if (this.websocket && this.websocket.readyState === 1) {
        this.send('');
      }
    }, 5000);
    this.isConnected$.next(true);

    this.websocket.onopen = e => {
      this.isOpen$.next(e);
      this.isConnected$.next(false);
      this.isConnecting$.next(true);
    };

    this.websocket.onmessage = e => {
      this.isMessage$.next(e);
    };

    this.websocket.onclose = e => {
      this.isClose$.next(e);
      this.isConnecting$.next(false);
    };

    this.websocket.onerror = e => {
      this.isError$.next(e);
    };
  }

  send(message: string): void {
    if (this.websocket && this.websocket.readyState === 1) {
      this.websocket.send(message);
    }
  }

  close(): void {
    if (this.websocket && this.websocket.readyState === 1) {
      this.websocket.close();
      clearInterval(this.pingSetInterval);
    }
  }
}
