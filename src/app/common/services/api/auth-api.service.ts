import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthLoginResponseInterface } from '../../interfaces/api/auth-login-response.interface';
import { AuthLoginRequestInterface } from '../../interfaces/api/auth-login-request.interface';

@Injectable({ providedIn: 'root' })
export class AuthApiService {
  constructor(private http: HttpClient) {
  }

  login(model: AuthLoginRequestInterface): Observable<AuthLoginResponseInterface> {
    return this.http.post<any>('https://api.deala.technaxis.com/api/v1/sudo/auth', model);
  }
}
