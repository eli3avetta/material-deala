import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsersSearchRequest } from '../../interfaces/api/users-search-request.interface';
import { UsersSearchResponse } from '../../interfaces/api/users-search-response.interface';

@Injectable({providedIn: 'root'})
export class UsersApiService {
  constructor(private apiService: ApiService) {
  }

  searchUsers(model: UsersSearchRequest): Observable<UsersSearchResponse> {
    return this.apiService.post('https://api.deala.technaxis.com/api/v1/admin/users/search', model)
      .pipe(map(data => data.result));
  }
}
