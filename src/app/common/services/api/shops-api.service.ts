import { Injectable } from '@angular/core';
import { ShopsSearchRequest } from '../../interfaces/api/shops-search-request.interface';
import { Observable } from 'rxjs';
import { ShopsSearchResponse } from '../../interfaces/api/shops-search-response.interface';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ShopsApiService {
  constructor(private apiService: ApiService) {
  }

  searchShops(model: ShopsSearchRequest): Observable<ShopsSearchResponse> {
    return this.apiService.post('https://api.deala.technaxis.com/api/v1/sudo/shops/search', model)
      .pipe(map(data => data.result));
  }
}
