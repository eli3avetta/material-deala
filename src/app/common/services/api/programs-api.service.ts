import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProgramsSearchRequest } from '../../interfaces/api/programs-search-request.interface';
import { ProgramsSearchResponse } from '../../interfaces/api/programs-search-response.interface';

@Injectable({providedIn: 'root'})
export class ProgramsApiService {
  constructor(private apiService: ApiService) {
  }

  searchPrograms(model: ProgramsSearchRequest): Observable<ProgramsSearchResponse> {
    return this.apiService.post('https://api.deala.technaxis.com/api/v1/admin/affiliate-programs/search', model)
      .pipe(map(data => data.result));
  }
}
