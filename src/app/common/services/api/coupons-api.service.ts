import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';
import { CouponsSearchRequest } from '../../interfaces/api/coupons-search-request.interface';
import { CouponsSearchResponse } from '../../interfaces/api/coupons-search-response.interface';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CouponsApiService {
  constructor(private apiService: ApiService) {
  }

  searchCoupons(model: CouponsSearchRequest): Observable<CouponsSearchResponse> {
    return this.apiService.post('https://api.deala.technaxis.com/api/v1/sudo/coupons/search', model)
      .pipe(map(data => data.result));
  }
}
