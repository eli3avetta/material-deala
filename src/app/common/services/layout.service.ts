import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class LayoutService {
  sidenavIsShow$ = new BehaviorSubject(true);

  setSidenavShow(flag: boolean): void {
    if (this.sidenavIsShow$.value !== flag) {
      this.sidenavIsShow$.next(flag);
    }
  }
}
