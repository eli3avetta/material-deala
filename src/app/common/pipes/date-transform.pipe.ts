import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTransform'
})
export class DateTransformPipe implements PipeTransform {
  transform(dateMs: number): string {
    if (dateMs) {
      const today = new Date(dateMs);

      let day: string | number = today.getDate();
      if (day < 10) {
        day = `0${ day }`;
      }

      let month: string | number = today.getMonth() + 1;
      if (month < 10) {
        month = `0${ month }`;
      }

      const year = today.getFullYear();

      return `${ day }.${ month }.${ year }`;
    }
  }
}
