import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'linkTransform'
})
export class LinkTransformPipe implements PipeTransform{
  transform(link: string): string {
    if (link) {
      if (link.includes('https://')) {
        return link;
      } else {
        return `https://${ link }`;
      }
    }
  }
}
