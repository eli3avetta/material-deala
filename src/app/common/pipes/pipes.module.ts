import { NgModule } from '@angular/core';
import { LinkTransformPipe } from './link-transform.pipe';
import { DateTransformPipe } from './date-transform.pipe';
import { DecimalInPercentPipe } from './decimal-in-percent.pipe';
import { UpperCaseInStandard } from './upper-case-in-standard';

@NgModule({
  declarations: [
    LinkTransformPipe,
    DateTransformPipe,
    DecimalInPercentPipe,
    UpperCaseInStandard
  ],
  exports: [
    LinkTransformPipe,
    DateTransformPipe,
    DecimalInPercentPipe,
    UpperCaseInStandard
  ]
})
export class PipesModule {
}
