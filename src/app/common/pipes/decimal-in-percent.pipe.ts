import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalInPercent'
})
export class DecimalInPercentPipe implements PipeTransform {
  transform(decimal: number): string {
    if (decimal && decimal < 1) {
      return `${ decimal * 100}%`;
    }
  }
}
