import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'upperCaseInStandard'
})
export class UpperCaseInStandard implements PipeTransform {
  transform(value: string): string {
    return value[0].toUpperCase() + value.substring(1, value.length).toLowerCase();
  }
}
