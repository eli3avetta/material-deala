import { UserModelInterface } from '../user-model.interface';
import { CommonSearchResponse } from './common-search-response';
import { ProgramModelInterface } from './program-model.interface';

export interface ProgramsSearchResponse extends CommonSearchResponse<ProgramModelInterface[]> {
}
