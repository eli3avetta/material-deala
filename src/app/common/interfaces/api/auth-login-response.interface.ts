export interface AuthLoginResponseInterface {
  result: {
    authToken: string;
    role: string;
    userId: number;
  };
}
