export interface ProgramModelInterface {
  clicksCount: number;
  couponsCount: number;
  description: string;
  id: number;
  merchantsCount: number;
  priorityMerchantsCount: number;
  priorityOrder: number;
  title: string;
}
