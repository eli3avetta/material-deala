import { UserModelInterface } from '../user-model.interface';
import { CommonSearchResponse } from './common-search-response';

export interface UsersSearchResponse extends CommonSearchResponse<UserModelInterface[]> {
}
