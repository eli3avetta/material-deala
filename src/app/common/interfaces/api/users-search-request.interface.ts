export interface UsersSearchRequest {
  limit: number;
  offset: number;
  sorting: [
    {
      order: 'ASC' | 'DESC';
      type: 'ID' | 'FIRST_NAME' | 'LAST_NAME' | 'ACCEPTED_COUPONS' | 'DECLINED_COUPONS' | 'TOTAL_PROCESSED_COUPONS'
    }
  ];
}
