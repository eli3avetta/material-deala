import { CouponModelInterface } from '../coupon-model.interface';
import { CommonSearchResponse } from './common-search-response';

export interface CouponsSearchResponse extends CommonSearchResponse<CouponModelInterface[]> {
}
