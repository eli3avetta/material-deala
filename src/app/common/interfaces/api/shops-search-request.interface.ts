export interface ShopsSearchRequest {
  limit: number;
  offset: number;
}
