import { ShopModelInterface } from '../shop-model.interface';
import { CommonSearchResponse } from './common-search-response';

export interface ShopsSearchResponse extends CommonSearchResponse<ShopModelInterface[]> {
  ranges: {};
}
