export interface CommonSearchResponse<T> {
  data: T;
  totalItems: number;
  totalPages: number;
}
