export interface CouponsSearchRequestSorting {
  order: 'ASC' | 'DESC';
  type: 'ID' | 'CREATED_DATE' | 'EXPIRATION_DATE' | 'CLICKS_COUNT' | 'LAST_USED_DATE' |
    'SUCCESS_RATE' | 'ADDED_TO_MODERATION_DATE_TIME' | 'STATUS';
}

export interface CouponsSearchRequest {
  limit?: number;
  offset?: number;
  query?: string;
  fromMS?: number;
  toMS?: number;
  affiliateProgramIds?: number[];
  withAffiliatePrograms?: boolean;
  creatorId?: number;
  status?: string;
  withSuccessRate?: boolean;
  sorting?: CouponsSearchRequestSorting[];
}
