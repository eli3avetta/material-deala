export interface ProgramsSearchRequest {
  limit: number;
  sorting: [
    {
      order: 'ASC' | 'DESC';
      type: 'PRIORITY_ORDER'
    }
  ];
}
