export interface AuthLoginRequestInterface {
  email: string;
  password: string;
}
