export interface UserModelInterface {
  acceptedCoupons: number;
  declinedCoupons: number;
  email: string;
  firstName: string;
  id: number;
  lastName: string;
  phoneNumber: string;
  role: 'ADMIN' | 'MODERATOR';
  totalProcessedCoupons: number
}
