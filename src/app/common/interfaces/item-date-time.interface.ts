export interface ItemDateTimeInterface {
  hours: number;
  minutes: number;
  text: string;
}
