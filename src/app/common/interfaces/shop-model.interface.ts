export interface AffiliateProgramInShopInterface {
  active: boolean;
  id: number;
  link: string;
  priority: number;
  title: string;
}

export interface ShopModelInterface {
  id: number;
  title: string;
  couponsCount: number;
  visitorsCount: number;
  successRate: number;
  totalCouponsClickCount: number;
  createDateTimeMS: number;
  lastCouponAddTime: number;

  creator: {
    firstName: string;
    id: number;
    lastName: string;
  };
  link: string;
  logo: {
    createdDateTimeInMillis: number;
    fileSize: number;
    height: number;
    id: number;
    name: string;
    order: number;
    ownerId: number;
    previewUrl: string;
    type: string;
    url: string;
    width: number;
  };
  affiliateProgram: AffiliateProgramInShopInterface;
  affiliatePrograms: AffiliateProgramInShopInterface[];
}
