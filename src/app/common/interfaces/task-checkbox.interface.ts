import { ThemePalette } from '@angular/material/core';

export interface Task {
  id: number;
  title: string;
  completed: boolean;
  color: ThemePalette;
  main?: boolean;
  subtasks?: Task[];
}
