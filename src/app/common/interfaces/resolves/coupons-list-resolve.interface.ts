import { CouponsSearchResponse } from '../api/coupons-search-response.interface';
import { UsersSearchResponse } from '../api/users-search-response.interface';
import { ProgramsSearchResponse } from '../api/programs-search-response.interface';

export interface CouponsListResolveInterface {
  couponsData: CouponsSearchResponse;
  programsData: ProgramsSearchResponse;
  usersData: UsersSearchResponse;
  fromMS: number;
  toMS: number;
}
