export interface CouponModelInterface  {
  clicksCount: number;
  code: string;
  createdDateTimeMS: number;
  creator: {
    firstName: string;
    id: number;
    lastName: string;
  };
  description: string;
  expirationDateTimeMS: number;
  id: number;
  lastUsedDateTimeMS: number;
  rejectReason: string;
  shop: {
    activeAffiliateProgram: {
      active: true;
      id: number;
      link: string;
      priority: number;
      title: string;
    };
    createDateTimeMS: number;
    id: number;
    link: string;
    title: string;
  };
  source: {
    channelLink: string;
    id: number;
    name: string;
    postLink: string;
    postText: string;
    type: string; // CUSTOM
  };
  status: string; // ACTIVE
  successRate: number;
  title: string;
  validationDateTimeMS: number;
  validatorId: number;
}
