export interface SidenavItem {
  sectionName: string;
  pages: Page[];
}

interface Page {
  pageName: string;
  pageLink: string;
}
