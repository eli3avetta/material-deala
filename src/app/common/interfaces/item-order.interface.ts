export interface ItemOrderInterface {
  id: number;
  title: string;
  text?: string;
  imageLink: string;
  prise: number;
  calories: number;
}
