export function calculatedColumnsWidth(columnsWidth): void {
  let totalWidth = 0;
  Object.keys(columnsWidth).forEach(key => {
    totalWidth += columnsWidth[key];
  });

  Object.keys(columnsWidth).forEach(key => {
    columnsWidth[key] = columnsWidth[key] / totalWidth * 100;
  });

  return columnsWidth;
}
