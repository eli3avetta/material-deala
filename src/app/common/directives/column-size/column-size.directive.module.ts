import { NgModule } from '@angular/core';
import { ColumnSizeDirective } from './column-size.directive';

@NgModule({
  declarations: [ColumnSizeDirective],
  exports: [ColumnSizeDirective]
})
export class ColumnSizeDirectiveModule {
}
