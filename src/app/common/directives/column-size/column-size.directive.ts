import { Directive, ElementRef, Input, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appColumnSize]'
})
export class ColumnSizeDirective implements OnInit {
  @Input() columnSize: number;

  constructor(private renderer: Renderer2,
              private el: ElementRef) {
  }

  ngOnInit(): void {
    this.renderer.setStyle(this.el.nativeElement, 'max-width', `${ this.columnSize }%`);
    this.renderer.setStyle(this.el.nativeElement, 'flex', '1 1 100%');
  }
}
