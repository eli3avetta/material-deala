import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';
import { IsBrowserService } from '@common/services/is-browser.service';

@Directive({
  selector: '[appDisableControl]'
})
export class DisableControlDirective {
  @Input() set appDisableControl(condition: boolean) {
    if (this.isBrowserService.isBrowser) {
      const action = condition ? 'disable' : 'enable';
      if (this.ngControl.control) {
        this.ngControl.control[action]();
      } else {
        setTimeout(() => {
          this.ngControl.control[action]();
        });
      }
    }
  }

  constructor(private ngControl: NgControl,
              private isBrowserService: IsBrowserService) {
  }
}
