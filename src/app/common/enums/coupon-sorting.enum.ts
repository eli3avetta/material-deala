export enum CouponSortingOrder {
  Asc = 'ASC',
  Desk = 'DESC'
}

export enum CouponSortingType {
  Id = 'ID',
  CreatedDate = 'CREATED_DATE',
  ExpirationDate = 'EXPIRATION_DATE',
  ClicksCount = 'CLICKS_COUNT',
  LastUsedDate = 'LAST_USED_DATE',
  SuccessRate = 'SUCCESS_RATE',
  AddedToModerationDateTime = 'ADDED_TO_MODERATION_DATE_TIME',
  Status = 'STATUS',
}
