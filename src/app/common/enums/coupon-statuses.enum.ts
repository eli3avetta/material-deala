export enum CouponStatusesEnum {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE',
  Rejected = 'REJECTED',
  Moderation = 'MODERATION'
}
